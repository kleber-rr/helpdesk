package com.helpdesk.api.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.helpdesk.api.entity.ChangeStatus;
import com.helpdesk.api.entity.Ticket;

@Component
public interface TicketService {

	Ticket createOrUpdate(Ticket ticket);
	
	Optional<Ticket> findById(String id);
	
	void delete(String id);
	
	Page<Ticket> listTicket(int page, int count, Direction sort, String field);
	
	ChangeStatus createChangeStatus(ChangeStatus changeStatus);
	
	Iterable<ChangeStatus> listChangeStatus(String ticketId);
	
	Page<Ticket> findByCurrentUser(int page, int count, String userId, Direction sort, String field);
	
	Page<Ticket> findByParameters(int page, int count,String title,String status,String priority, Direction sort, String field);
	
	Page<Ticket> findByParametersAndCurrentUser(int page, int count,String title,String status,String priority,String userId, Direction sort, String field);
	
	Page<Ticket> findByNumber(int page, int count,Integer number, Direction sort, String field);
	
	Iterable<Ticket> findAll();
	
	public Page<Ticket> findByParametersAndAssignedUser(int page, int count,String title,String status,String priority,String assignedUserId, Direction sort, String field);
}