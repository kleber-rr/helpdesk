package com.helpdesk.api.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.helpdesk.api.entity.ChangeStatus;
import com.helpdesk.api.entity.Ticket;
import com.helpdesk.api.repository.ChangeStatusRepository;
import com.helpdesk.api.repository.TicketRepository;
import com.helpdesk.api.service.TicketService;

@Component
public class TicketServiceImpl implements TicketService {
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private ChangeStatusRepository changeStatusRepository;

	public Ticket createOrUpdate(Ticket ticket) {
		return this.ticketRepository.save(ticket);
	}

	public Optional<Ticket> findById(String id) {
		return this.ticketRepository.findById(id);
	}

	public void delete(String id) {
		this.ticketRepository.deleteById(id);
	}

	public Page<Ticket> listTicket(int page, int count, Direction sort, String field) {
		PageRequest pages = PageRequest.of(
                page,
                count,
				sort,
				field);
		return this.ticketRepository.findAll(pages);
	}
	
	public Iterable<Ticket> findAll() {
		return this.ticketRepository.findAll();
	}
	
	public Page<Ticket> findByCurrentUser(int page, int count, String userId, Direction sort, String field) {
		PageRequest pages = PageRequest.of(
                page,
                count,
				sort,
				field);
		return this.ticketRepository.findByUserIdOrderByDateDesc(pages,userId);
	}

	public ChangeStatus createChangeStatus(ChangeStatus changeStatus) {
		return this.changeStatusRepository.save(changeStatus);
	}
	
	public Iterable<ChangeStatus> listChangeStatus(String ticketId) {
		return this.changeStatusRepository.findByTicketIdOrderByDateChangeStatusDesc(ticketId);
	}
	
	public Page<Ticket> findByParameters(int page, int count,String title,String status,String priority, Direction sort, String field) {
		PageRequest pageRequest = PageRequest.of(
                page,
                count,
				sort,
				field);
		return this.ticketRepository.
				findByTitleIgnoreCaseContainingAndStatusContainingAndPriorityContainingOrderByDateDesc(
				title, status, priority, pageRequest);
	}
	
	public Page<Ticket> findByParametersAndCurrentUser(int page, int count,String title,String status,
			String priority,String userId, Direction sort, String field) {
		PageRequest pages = PageRequest.of(
                page,
                count,
				sort,
				field);
		return this.ticketRepository.
				findByTitleIgnoreCaseContainingAndStatusContainingAndPriorityContainingAndUserIdOrderByDateDesc(
				title,status,priority,userId,pages);
	}
	
	public Page<Ticket> findByNumber(int page, int count,Integer number, Direction sort, String field) {
		PageRequest pages = PageRequest.of(
                page,
                count,
				sort,
				field);
		return this.ticketRepository.findByNumber(number, pages);
	}
	
	public Page<Ticket> findByParametersAndAssignedUser(int page, int count,String title,String status,
			String priority,String assignedUserId, Direction sort, String field) {
				PageRequest pages = PageRequest.of(
						page,
						count,
						sort,
						field);
		return this.ticketRepository.
				findByTitleIgnoreCaseContainingAndStatusContainingAndPriorityContainingAndAssignedUserIdOrderByDateDesc(
				title,status,priority,assignedUserId,pages);
	}
}
