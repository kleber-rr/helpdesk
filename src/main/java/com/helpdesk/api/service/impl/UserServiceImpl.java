package com.helpdesk.api.service.impl;

import java.util.Optional;

import com.helpdesk.api.entity.User;
import com.helpdesk.api.repository.UserRepository;
import com.helpdesk.api.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User findByEmail(String email) {
		return this.userRepository.findByEmail(email);
	}

	@Override
	public User createOrUpdate(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public Optional<User> findById(String id) {
		return this.userRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		this.userRepository.deleteById(id);
	}

	@Override
	public Page<User> findAll(int page, int count, Direction dir, String field) {
		PageRequest pageRequest = PageRequest.of(
                page,
                count,
				dir,
				field);
		return this.userRepository.findAll(pageRequest);
	}

}
