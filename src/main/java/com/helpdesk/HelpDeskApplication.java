package com.helpdesk;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.helpdesk.api.entity.User;
import com.helpdesk.api.enums.ProfileEnum;
import com.helpdesk.api.repository.UserRepository;

@SpringBootApplication
public class HelpDeskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpDeskApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(UserRepository repos, PasswordEncoder encoder) {
		return args -> {
			initUsers(repos, encoder);
		};
	}

	private void initUsers(UserRepository repos, PasswordEncoder encoder) {
		User admin = new User();
		admin.setEmail("admin@admin");
		admin.setPassword(encoder.encode("123"));
		admin.setProfile(ProfileEnum.ROLE_ADMIN);
		
		User find = repos.findByEmail("admin@admin");
		if(find == null) {
			repos.save(admin);
		}
	}

}
